// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  runtimeConfig: {
    public: {
      siteName: 'Церковь Христа', // can be overridden by NUXT_PUBLIC_SITE_NAME environment variable
    }
  },
  css: [
    '~/assets/styles/boot.scss',
    '@fortawesome/fontawesome-svg-core/styles.css',
  ],
  devtools: { enabled: true }
})
